import { FETCH_HEXAGONS } from '../constants/ActionTypes';

const initialState = {
  content: null,
  loading: false,
  params: {
    grade: 1,
    level: 'Secundaria'
  }
};

/**
 * Hexagons reducer
 * @method hexagons
 * @param  {object} [state=initialState] Current state
 * @param  {object} action               Action object
 * @return {object}                      Next state
 */
export default function hexagons(state = initialState, action) {
  switch (action.type) {
    case FETCH_HEXAGONS.PENDING:
      return {
        ...state,
        loading: true
      };
    case FETCH_HEXAGONS.FULFILLED:
      return {
        ...state,
        loading: false,
        content: action
          .payload
          .data
          .data
          .sort((a, b) => parseInt(a.file_index, 10) - parseInt(b.file_index, 10))
      }
    case FETCH_HEXAGONS.REJECTED:
      return {
        ...state,
        loading: false,
        content: new Error('No content')
      }
    case FETCH_HEXAGONS.UPDATE:
      const params = {};
      const paramsType = [
        'level',
        'grade',
        'lesson',
        'bimester'
      ];
      // Iterate over each param to assign value
      _.forEach(paramsType, (param) => {
        if(action.params[param] && action.params[param] !== ''){
          params[param] = action.params[param];
        }
      });
      return {
        ...state,
        params
      }
    default:
      return state;
  }
}
