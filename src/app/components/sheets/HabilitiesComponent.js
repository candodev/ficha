import React, {Component, PropTypes} from 'react';

class Habilities extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div className="reflection">
        <div className="table-opacity">
          <div className="index-opacity">
            <table className="table-valuations table-padding">
              <thead>
                <tr>
                  <th>Emoción</th>
                  <th>Promedio en el grupo</th>
                  <th>Detalle</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Comunicación</td>
                  <td>
                    <div className="percentage">
                      <div>0 de 30</div>
                      <div>
                        <div className="progress-bar">
                          <div className="progress" style={{width: '0%'}}></div>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td>
                    <i className="fa fa-eye" aria-hidden="true"></i>
                  </td>
                </tr>
                <tr>
                  <td>Colaboración</td>
                  <td>
                    <div className="percentage">
                      <div>0 de 30</div>
                      <div>
                        <div className="progress-bar">
                          <div className="progress" style={{width: '0%'}}></div>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td>
                    <i className="fa fa-eye" aria-hidden="true"></i>
                  </td>
                </tr>
                <tr>
                  <td>Creatividad</td>
                  <td>
                    <div className="percentage">
                      <div>0 de 30</div>
                      <div>
                        <div className="progress-bar">
                          <div className="progress" style={{width: '0%'}}></div>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td>
                    <i className="fa fa-eye" aria-hidden="true"></i>
                  </td>
                </tr>
                <tr>
                  <td>Resolución de<br/>Problemas</td>
                  <td>
                    <div className="percentage">
                      <div>0 de 30</div>
                      <div>
                        <div className="progress-bar">
                          <div className="progress" style={{width: '0%'}}></div>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td>
                    <i className="fa fa-eye" aria-hidden="true"></i>
                  </td>
                </tr>
                <tr>
                  <td>Manejo de<br/>información</td>
                  <td>
                    <div className="percentage">
                      <div>0 de 30</div>
                      <div>
                        <div className="progress-bar">
                          <div className="progress" style={{width: '0%'}}></div>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td>
                    <i className="fa fa-eye" aria-hidden="true"></i>
                  </td>
                </tr>
                <tr>
                  <td>Uso de medios</td>
                  <td>
                    <div className="percentage">
                      <div>0 de 30</div>
                      <div>
                        <div className="progress-bar">
                          <div className="progress" style={{width: '0%'}}></div>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td>
                    <i className="fa fa-eye" aria-hidden="true"></i>
                  </td>
                </tr>
                <tr>
                  <td>Manejo de<br/>tecnología</td>
                  <td>
                    <div className="percentage">
                      <div>0 de 30</div>
                      <div>
                        <div className="progress-bar">
                          <div className="progress" style={{width: '0%'}}></div>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td>
                    <i className="fa fa-eye" aria-hidden="true"></i>
                  </td>
                </tr>
                <tr>
                  <td>Curiosidad</td>
                  <td>
                    <div className="percentage">
                      <div>0 de 30</div>
                      <div>
                        <div className="progress-bar">
                          <div className="progress" style={{width: '0%'}}></div>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td>
                    <i className="fa fa-eye" aria-hidden="true"></i>
                  </td>
                </tr>
                <tr>
                  <td>Iniciativa</td>
                  <td>
                    <div className="percentage">
                      <div>0 de 30</div>
                      <div>
                        <div className="progress-bar">
                          <div className="progress" style={{width: '0%'}}></div>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td>
                    <i className="fa fa-eye" aria-hidden="true"></i>
                  </td>
                </tr>
                <tr>
                  <td>Persistencia</td>
                  <td>
                    <div className="percentage">
                      <div>0 de 30</div>
                      <div>
                        <div className="progress-bar">
                          <div className="progress" style={{width: '0%'}}></div>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td>
                    <i className="fa fa-eye" aria-hidden="true"></i>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="table-info">
            <img src="/assets/images/temporary/Calendario.png" className="img-calendar" />
            <p className="txt-info">Esta tabla se empezará a llenar con las<br/>respuestas que ingresen tus alumnos</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Habilities;
