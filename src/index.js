import 'babel-polyfill';

import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import App from './app/containers/App';
import Ficha from './app/containers/Ficha';
import Path from './app/containers/Path';
import configureStore from './app/store/configureStore';
import {Router, Route, browserHistory, hashHistory} from 'react-router';
import injectTapEventPlugin from 'react-tap-event-plugin';
// Main style sheet
import './index.scss';


injectTapEventPlugin();

const store = configureStore();

render(
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path="/" component={App}/>
      <Route path="/ficha/:fichaId" component={Ficha}/>
      <Route path="/path" component={Path}/>
    </Router>
  </Provider>,
  document.getElementById('root')
);
