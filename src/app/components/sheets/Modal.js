import React, {Component, PropTypes} from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import DatePicker from 'material-ui/DatePicker';
import TextField from 'material-ui/TextField';
import FontIcon from 'material-ui/FontIcon';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

const styles = {
  customWidth: {
    width: 650,
    marginLeft: 600,
  },
};

export class Modal extends Component {

	state = {
    open: false,
    value: 1
  };

  handleChange = (event, index, value) => this.setState({value});

  render() {
    const actions = [
      <FlatButton
        label="Cancelar"
        primary={true}
        onTouchTap={this.props.handleClose}
        labelStyle={{color: '#fff'}}
      />,
      <FlatButton
        label="Enviar"
        primary={true}
        onTouchTap={this.props.handleClose}
        labelStyle={{color: '#fff'}}
        backgroundColor="#3848B0"
      />,                   
    ];

    return(
      <div >
        <Dialog 
          actions={actions}  
          modal={false}
          open={this.props.modalOpen}
          onRequestClose={this.props.handleClose}
          autoScrollBodyContent={true}
          style={styles.customWidth}
          >
          <div className="modal">
            <span onClick={this.props.handleClose}>&times;</span>
            <h1>Agregue un Recurso Educativo Digital para compartir con sus alumnos en esta ficha.</h1>
            <p> Video, imagen, sitio web, archivo, etc.</p>
          </div>
          <div className="modal-inputs">
            <TextField
              hintText="Escribir un Titulo"
              className="input-modal"
              style={{backgroundColor:'#EBEFF2', borderRadius: '5px', paddingLeft: '5px', marginTop: '35px'}}
              fullWidth={true}
              underlineStyle={{borderColor: '#EBEFF2'}}
            /><br />
            <h3>Video</h3>
            <TextField
              hintText="https://www.youtube.com/watch?v=UmhFSxyvVFw"
              style={{backgroundColor:'#EBEFF2', borderRadius: '5px', paddingLeft: '5px', marginBottom: '15px'}}
              fullWidth={true}
              underlineStyle={{borderColor: '#EBEFF2'}}
            /><br />
            <h3>Archivo</h3>
            <TextField
              hintText="Seleccionar un archivo"
              style={{backgroundColor:'#EBEFF2', borderRadius: '5px', paddingLeft: '5px', marginBottom: '25px'}}
              fullWidth={true}
              underlineStyle={{borderColor: '#EBEFF2'}}
            /><br />
            <span className="text">
              <i className="fa fa-plus-circle" aria-hidden="true"></i>
              Agregar otro link o archivo</span>
            <br />
            <SelectField
              floatingLabelText="¿En que seccion quieres agregarlo?"
              value={this.state.value}
              onChange={this.handleChange}
              style={{backgroundColor:'#EBEFF2', borderRadius: '5px', paddingLeft: '5px', marginTop: '25px'}}
              fullWidth={true}
            >
              <MenuItem value={1} primaryText="Descubre" />
              <MenuItem value={2} primaryText="Idea" />
              <MenuItem value={3} primaryText="Crea"/>
            </SelectField>
            <br />
          </div>
        </Dialog>
      </div>
    );
  }
}

export default Modal;
