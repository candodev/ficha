import { FETCH_SHEET } from '../constants/ActionTypes';

const initialState = {
  content: null,
  loading: false
};

/**
 * Sheet reducer
 * @method sheet
 * @param  {object} [state=initialState] Current state
 * @param  {object} action               Action object
 * @return {object}                      Next state
 */
export default function sheet(state = initialState, action) {
  switch (action.type) {
    case FETCH_SHEET.PENDING:
      return {
        ...state,
        loading: true
      };
    case FETCH_SHEET.FULFILLED:
      return {
        ...state,
        loading: false,
        content: action.payload.data
      }
    default:
      return state;
  }
}
