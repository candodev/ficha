import React, {Component, PropTypes} from 'react';

export class Enhance extends Component {
  constructor(props, context) {
    super(props, context);
  }

  createMarkup(htmlContent) {
    return {__html: htmlContent};
  }

  render() {
    const content = this.props.enrichContent;
    return (
      <div>
        {content.related_plannings ? (<div>
          <div>
            <h4 className="title-h4">PLANEACIONES RELACIONADAS</h4>
          </div>
          <table className="table-pla-rel">
            <thead>
              <tr>
                <th>PLAN</th>
                <th>Tema</th>
                <th>Aprendizaje</th>
                <th>Ver</th>
              </tr>
            </thead>
            <tbody>
            {content.related_plannings.map((plannings, plann) => {
                return(
                  <tr key={plann}>
                    <td className="title">{plannings.enrich_plan_title}</td>
                    <td dangerouslySetInnerHTML={this.createMarkup(plannings.enrich_plan_theme)}></td>
                    <td>{plannings.enrich_plan_learning}</td>
                    <td>
                      {plannings.enriche_plan_url ? (
                        <a href={plannings.enriche_plan_url}>
                          <i className="fa fa-eye" aria-hidden="true"></i>
                        </a>
                        ) : <i className="fa fa-eye" aria-hidden="true"></i>}
                    </td>
                </tr>
                )
              })}
            </tbody>
          </table>
        </div>) : null}
        {content.related_reactives ? (<div>
          <div>
            <h4 className="title-h4">REACTIVOS RELACIONADOS</h4>
          </div>
          <table className="table-rea-rel">
            <thead>
              <tr>
                <th className="th-border">Reactivo</th>
                <th>Respuestas</th>
                <th>Agregar</th>
              </tr>
            </thead>
            <tbody>
            {content.related_reactives.map((reactives, reac) => {
                return(
                  <tr key={reac}>
                    <td>
                      <p>{reactives.enrich_reactive_title}</p>
                    </td>
                    <td>
                      <p
                        className="correct"
                        dangerouslySetInnerHTML={
                          this.createMarkup(reactives.enrich_reactive_answers)
                        }></p>
                    </td>
                    <td>
                      <img src="/assets/images/temporary/work-sheets/more.png" />
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>) : null}
        {content.related_meds ? (<div>
          <div>
            <h4 className="title-h4">MED RELACIONADOS</h4>
          </div>
          <table className="table-med-rel">
            <thead>
              <tr>
                <th className="th-border">Título</th>
                <th>Descripción</th>
                <th >Ver</th>
                <th>Agregar</th>
              </tr>
            </thead>
            <tbody>
            {content.related_meds.map((obj, enrich) => {
                return(
                  <tr key={enrich}>
                    <td className="title">
                      <div className="meds-set">
                        <div>
                          <p>{obj.enrich_med_title}</p>
                        </div>
                      </div>
                    </td>
                    <td>
                      <p dangerouslySetInnerHTML={this.createMarkup(obj.enrich_med_desc)}></p>
                    </td>
                    <td>                      
                      {obj.enrich_med_url ? (
                        <a href={obj.enrich_med_url}>
                          <i className="fa fa-eye" aria-hidden="true"></i>
                        </a>) : <i className="fa fa-eye" aria-hidden="true"></i>}
                    </td>
                    <td>
                      <img src="/assets/images/temporary/work-sheets/more.png" />
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>) : null}
      </div>
    );
  }
}

Enhance.propTypes = {
  enrichContent: PropTypes.object.isRequired
};

export default Enhance;
