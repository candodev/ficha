import React, {Component, PropTypes} from 'react';
import {Card} from 'material-ui/Card';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Header} from './../components/potencia/header';
import {SidebarMenu} from './../components/potencia/sidebar-menu';
import {Hexagon} from './../components/potencia/hexagon';
import {browserHistory, hashHistory} from 'react-router';
import {connect} from 'react-redux';
import * as Actions from '../actions/index';
import {bindActionCreators} from 'redux';
import './path.scss';

const MIN_BIMESTER = 1;
const MAX_BIMESTER = 5;

export class Path extends Component {
  /**
   * Constructor for path component
   * @param  {object} props Props object
   */
  constructor(props) {
    super(props);
    this.state = {
      isOpenModal: true,
      bimester: 1
    };
    this.romanNumbers = {
      1: 'I',
      2: 'II',
      3: 'III',
      4: 'IV',
      5: 'V'
    };
    this.actions = this.props.actions;
    //this.handleGo = this.handleGo.bind(this);
  }

  /**
   * Go to ficha profile
   * @param  {number} fichaId Ficha ID
   */
  handleGo(fichaId) {
    hashHistory.push(`/ficha/${fichaId}`);
  }

  createMarkup(htmlContent) {
    return {__html: htmlContent};
  }

  /**
   * componentDidMount lifecycle method
   */
  componentDidMount() {
    const {location} = this.props;
    // Get hexagons data
    if(location.query && location.query.bimester) {
      this.setState({bimester: parseInt(location.query.bimester, 10)});
    }
    this.actions.updateSearchParams(location.query);
  }

  /**
   * Get array of hexagons in groups of 3 elements
   * @return {array} [description]
   */
  getExagonsRows() {
    const {hexagons} = this.props;
    // Split content of array in groups of 3 elements or return false
    const response = (hexagons.content && hexagons.content.message)
      ? false
      : _.chunk(hexagons.content, 3);
    return response;
  }

  /**
   * Set status for modal aler
   * @param {boolean} status Status for modal alert
   */
  setStatusAlertModal(status) {
    this.setState({
      isOpenModal: status
    });
  }

  /**
   * Print hexagon rows
   * @param  {array} hexagonsContent Array of hexagons in groups of 3 elements
   * @return {ReactElement}          Hexagons rows markup
   */
  printHexagonRows(hexagonsContent) {
    return <div className="hexagons-wrapper subjects">
      {hexagonsContent.map((hexagonRow, i) => {
        let circleTag = (i !== hexagonsContent.length-1) ?
          <div className="circle"></div> : '';
        return ([<div className="hexagons-row" key={i}>
            {
              hexagonRow.map((hexagon) => {
                return this.printHexagon(hexagon);
              })
            }
          </div>,
          circleTag
        ])
      })}
    </div>
  }

  /**
   * Print hexagon
   * @param  {object} hexagon Hexagon info object
   * @return {ReactElement}   Hexagon markup
   */
  printHexagon(hexagon) {
    const {hexagons} = this.props;
    let subject = 'hex-blue-nohover';
    if(hexagons.params && hexagons.params.lesson) {
      switch(hexagons.params.lesson) {
        case 'Español':
          subject = 'hex-blue-nohover';
          break;
        case 'Matemáticas':
          subject = 'hex-orange-nohover';
          break;
        case 'Exploración':
          subject = 'hex-green-nohover';
          break;
      }
    }
    return <div className="hexagons-col" key={hexagon.ID}>
      <div
        className="hexagon-base cursor-pointer active"
        onClick={() => this.handleGo(hexagon.ID)}>
        <Hexagon
          anotherClass={subject}
          backgroundImage={hexagon.file_thumbnail ? `url('${hexagon.file_thumbnail}')` : ''}>
          <div className="hexagon-content">
          </div>
          {this.card(hexagon)}
        </Hexagon>
      </div>
      <div className="hex-info">
        <p className="description">{hexagon.post_title}</p>
        <p className="duration">&nbsp;</p>
      </div>
    </div>;
  }

  /**
   * Display alert modal
   * @return {ReactElement} Dialog alert markup
   */
  displayAlert() {
    // Actions buttons for dialog
    const action = [
      <FlatButton
        label="Aceptar"
        primary={true}
        onTouchTap={() => this.setStatusAlertModal(false)}
      ></FlatButton>
    ];
    return (
      <div>
        <Dialog
          actions={action}
          modal={false}
          open={this.state.isOpenModal}
          onRequestClose={() => this.setStatusAlertModal(false)}
        >
          No hay contenido para este modulo.
        </Dialog>
      </div>
    );
  }

  /**
   * Converts an object into a query string
   * @param {object} query Object that containts the elements of the query
   *
  */
  serialize(obj) {
    const str = [];
    for(let p in obj)
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
      }
    return str.join("&");
  }

  /**
   * Go to a specific bimester
   * @param  {string} position Position to go (next or back)
   */
  goToBimester(position) {
    const {location} = this.props;
    const limitBack = (
      this.state.bimester === MIN_BIMESTER && position === 'back'
    );
    const limitNext = (
      this.state.bimester === MAX_BIMESTER && position === 'next'
    );
    if (limitBack || limitNext) {
      return;
    }
    const bimesterUpdated = this.state.bimester + (
      position === 'back'
        ? - 1
        : 1
    );
    this.setState({
      bimester: bimesterUpdated,
    });
    // Update hexagons with new bimester updated and update hash history

    const query = {
      ...location.query,
      bimester: bimesterUpdated
    };
    this.actions.updateSearchParams(query);
    hashHistory.push(`/path?${this.serialize(query)}`);
  }

  card(info) {
    return (
      <div className="hexagon-info">
        <Card>
          <div className="hexagon-info-content">
            {info.ambit ? (<div>
              <h1 className="title-h4">Eje</h1>
              <p className="info-p" dangerouslySetInnerHTML={this.createMarkup(info.ambit)}></p>
            </div>) : null}
            {info.expected_learning ? (<div>
              <h1 className="title-h4">Aprendizaje esperado:</h1>
              <p className="info-p" dangerouslySetInnerHTML={this.createMarkup(info.expected_learning)}></p>
            </div>) : null}
            {info.theme ? (<div>
            <h1 className="title-h4">Tema:</h1>
            <p className="info-p" dangerouslySetInnerHTML={this.createMarkup(info.theme)}></p>
            </div>) : null}
            {info.contents ? (<div>
              <h1 className="title-h4">Contenido:</h1>
              <p className="info-p" dangerouslySetInnerHTML={this.createMarkup(info.contents)}></p>
            </div>) : null}
          </div>
        </Card>
      </div>
    );
  }

  /**
   * Render method
   * @return {ReactElement} Path markup
   */
  render() {
    const hexagonsContent = this.getExagonsRows();
    const {hexagons, location} = this.props;
    let subject = '';
    let bimester = '';
    if(hexagons.params && hexagons.params.lesson) {
      subject = hexagons.params.lesson;
    }

    if(hexagons.params && hexagons.params.bimester) {
      bimester = this.romanNumbers[hexagons.params.bimester];
    }
    
    return (
      <MuiThemeProvider>
        <div>
          <Header query={location.query} />
          <div className="main-content">
            <div className="nav">
              <SidebarMenu />
            </div>
            <div className="main-info">
              <div className="blocks-header">
                <button
                  className="block-link-prev"
                  onClick={() => this.goToBimester('back')}>
                  <i className="fa fa-angle-left" aria-hidden="true"></i>
                </button>
                <button
                  className="block-link-next"
                  onClick={() => this.goToBimester('next')}>
                  <i className="fa fa-angle-right" aria-hidden="true"></i>
                </button>
                <h1>Bimestre {bimester} {subject}</h1>
              </div>
              {
                // Print hexagons or display alert if there is no content for
                // hexagons
                (!hexagonsContent)
                  ? this.displayAlert()
                  : this.printHexagonRows(hexagonsContent)
              }
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

Path.propTypes = {
  hexagons: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    hexagons: state.hexagons
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Path);
