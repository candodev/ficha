import React, {Component, PropTypes} from 'react';
import classNames from 'classnames';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Discover from '../components/sheets/DiscoverComponent';
import Evaluation from '../components/sheets/EvaluationComponent';
import Think from '../components/sheets/ThinkComponent';
import Create from '../components/sheets/CreateComponent';
import Habilities from '../components/sheets/HabilitiesComponent';
import Enhance from '../components/sheets/EnhanceComponent';
import SimpleVideo from '../components/sheets/SimpleVideoComponent';
import Footer from '../components/sheets/Footer';
import Modal from '../components/sheets/Modal';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as SheetActions from '../actions/index';
import CircularProgress from 'material-ui/CircularProgress';
import './style.scss';

export class Ficha extends Component {

  constructor(props) {
    super(props);
    this.state = {
      open: [true, false, false, false, false, false, false],
      modalOpen: false
    };
    this.romanNumbers = {
      1: 'I',
      2: 'II',
      3: 'III',
      4: 'IV',
      5: 'V'
    };
  }

  createMarkup(htmlContent) {
    return {__html: htmlContent};
  }

  card(info) {
    return (
      <div className="hexagon-info">
        <Card>
          <div className="hexagon-info-content">
            {info.ambit ? (<div>
              <h1 className="title-h4">Eje</h1>
              <p className="info-p" dangerouslySetInnerHTML={this.createMarkup(info.ambit)}></p>
            </div>) : null}
            {info.expected_learning ? (<div>
              <h1 className="title-h4">Aprendizaje esperado:</h1>
              <p className="info-p" dangerouslySetInnerHTML={this.createMarkup(info.expected_learning)}></p>
            </div>) : null}
            {info.theme ? (<div>
            <h1 className="title-h4">Tema:</h1>
            <p className="info-p" dangerouslySetInnerHTML={this.createMarkup(info.theme)}></p>
            </div>) : null}
            {info.contents ? (<div>
              <h1 className="title-h4">Contenido:</h1>
              <p className="info-p" dangerouslySetInnerHTML={this.createMarkup(info.contents)}></p>
            </div>) : null}
          </div>
        </Card>
      </div>
    );
  }

  toggleOpen(index) {
    let newArray = this.state.open.slice(0);
    newArray[index] = !newArray[index];
    this.setState({
      open: newArray
    });
  }

  componentDidMount() {
    const {actions} = this.props;
    const sheet = this.props.params.fichaId;
    actions.fetchSheet(sheet);
  }

  showLoading = () => {
    const {sheet} = this.props;
    const styles = {
      root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
      }
    };
    if (sheet.loading) {
      return (
        <div style={styles.root}>
          <CircularProgress size={60} thickness={7} />
        </div>
      )
    }
  }

  star(number) {
    const stars = [];
    for (let i = 1; i <= 5; i++) {
      const offNull = '/assets/images/temporary/work-sheets/starOff.png';
      const off = '/assets/images/temporary/work-sheets/starOff.png';
      const on = '/assets/images/temporary/work-sheets/starOn.png';
      if (number < i) {
        stars.push(<img key={i} src={off} />);
      } else if (number === null){
        stars.push(<img key={i} src={offNull} />);
      } else {
        stars.push(<img key={i} src={on} />);
      }
    }
    return stars;
  }

  videos = () => {
    const {sheet} = this.props;
    const videos = [];

    if(sheet.content) {
      if(sheet.content.idea && sheet.content.idea.content) {
        sheet.content.idea.content.forEach((element, index) => {
          videos.push(<SimpleVideo
            key={index}
            id={element.idea_additional_embed}
            name={element.idea_additional_videoname}
            />
          );
        });
      }

      if(sheet.content.create && sheet.content.create.content) {
        sheet.content.create.content.forEach((element, index) => {
          videos.push(<SimpleVideo
            key={index + 100}
            id={element.idea_additional_embed}
            name={element.idea_additional_videoname}
            />
          );
        });
      }
    }

    return videos;
  }

  render() {
    const {sheet} = this.props;
    const content = {
      discover: {},
      idea: {},
      create: {},
      enrich: {},
      evaluation_content: {},
      ...sheet.content
    };

    let title = '';
    if(content && content.post_title) {
      title = content.post_title;
    }

    let rate = '';
    if(content && content.score) {
      rate = content.score;
    }

    let bimester = '';
    if(content && content.bimester) {
      bimester = this.romanNumbers[parseInt(content.bimester, 10)]
    }

    let lesson = '';
    if(content && content.lesson) {
      lesson = content.lesson;
    }

    return (
      <MuiThemeProvider>
      <div className="main-container">
        {this.showLoading()}
        <h3 className="h3-title"><span>Bloque {bimester} {lesson} / </span>{title}</h3>
        <div className="line-wrapper line-width">
          <a href="#descubre">
            <div className="hexagon-wrapper">
              <img src="/assets/images/temporary/work-sheets/hexagonoDescubre.png" />
            </div>
          </a>
          <a href="#idea">
            <div className="hexagon-wrapper">
              <img src="/assets/images/temporary/work-sheets/idea.png" />
            </div>
          </a>
          <a href="#crea">
            <div className="hexagon-wrapper">
              <img src="/assets/images/temporary/work-sheets/hexagonoCrea.png" />
            </div>
          </a>
          <a href="#habilidades">
            <div className="hexagon-wrapper">
              <img src="/assets/images/temporary/work-sheets/hexagonoHabilidades.png" />
            </div>
          </a>
          <div>
            <div className="img-column">
              <div className="hexagons-flex">
                <a className="hexagon-base base-change clicked" onClick={this.toggleOpen.bind(this, 6)}>
                  <img src="/assets/images/icon/eye.png" />
                  {this.state.open[6]?<div>{this.card(content)}</div>:null}
                </a>
                <a href="#enhance">
                  <img src="/assets/images/icon/play.png" />
                </a>
              </div>
            </div>
            <div className="img-column-2">
              <div style={{display: 'flex'}}>
                <a href="#enhance">
                  <img src="/assets/images/icon/pdf.png" />
                </a>
                <button href="#enhance" onClick={() => this.setState({modalOpen: true})}>
                  <img src="/assets/images/icon/plus.png" />
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="div-border"></div>
        <div className="work-sheet-item">
          <div className="rectangle-style bar-blue" id="descubre">
            <div className="bar-txt">
              Descubre
            </div>
            <div className="toggle-info" onClick={this.toggleOpen.bind(this, 0)}>
              <i
                className={classNames('fa', 'bar-icon', this.state.open[0]?'fa-caret-up':'fa-caret-down')}
                aria-hidden="true"
              >
              </i>
            </div>
          </div>
          {this.state.open[0]?<Discover discoverContent={content.discover}/>:null}

          <div>
            <div className="div-border"></div>
            <div className="rectangle-style bar-pink" id="idea">
              <div className="bar-txt">
                Idea
              </div>
              <div className="toggle-info" onClick={this.toggleOpen.bind(this, 1)}>
                <i
                className={classNames('fa', 'bar-icon', this.state.open[1]?'fa-caret-up':'fa-caret-down')}
                aria-hidden="true"
                >
                </i>
              </div>
            </div>
            {this.state.open[1]?<Think thinkContent={content.idea}/>:null}

            <div className="div-border"></div>
            <div className="rectangle-style bar-green" id="crea">
              <div className="bar-txt">
                Crea
              </div>
              <div className="toggle-info" onClick={this.toggleOpen.bind(this, 2)}>
                <i
                className={classNames('fa', 'bar-icon', this.state.open[2]?'fa-caret-up':'fa-caret-down')}
                aria-hidden="true"
                >
                </i>
              </div>
            </div>
            {this.state.open[2]?<Create createContent={content.create}/>:null}

            <div className="div-border"></div>
            <div className="rectangle-style bar-yellow" id="habilidades">
              <div className="bar-txt">
                Mis habilidades del SXII
              </div>
              <div className="toggle-info" onClick={this.toggleOpen.bind(this, 3)}>
                <i
                className={classNames('fa', 'bar-icon', this.state.open[3]?'fa-caret-up':'fa-caret-down')}
                aria-hidden="true"
                >
                </i>
              </div>
            </div>
            {this.state.open[3]?<Habilities />:null}

            <div className="div-border"></div>
            <div className="rectangle-style bar-navi-blue" id="descubre">
              <img src="/assets/images/temporary/work-sheets/recomendaciones.png" />
              <div className="txt-basis" id="enhance">Enriquece tu clase</div>
              <div className="toggle-info" onClick={this.toggleOpen.bind(this, 4)}>
                <i
                className={classNames('fa', 'bar-icon', this.state.open[4]?'fa-caret-up':'fa-caret-down')}
                aria-hidden="true">
                </i>
              </div>
            </div>
            {this.state.open[4]?<Enhance enrichContent={content.enrich}/>:null}

            <div className="div-border"></div>
            <div className="rectangle-style bar-navi-blue" id="evaluacion">
              <div className="bar-txt">
                Evaluación fomativa
              </div>
              <div className="toggle-info" onClick={this.toggleOpen.bind(this, 5)}>
                <i
                  className={classNames('fa', 'bar-icon', this.state.open[5]?'fa-caret-up':'fa-caret-down')}
                  aria-hidden="true"
                >
                </i>
              </div>
            </div>
            {this.state.open[5]?<Evaluation evaluationContent={content.evaluation_content}/>:null}

            <div className="div-border"></div>
            {this.videos()}
            <div className="div-border"></div>
            <div className="stars-row">
              <h1>Calificación promedio de esta actividad</h1>
              {this.star(rate)}
            </div>
          </div>
        </div>
        <div className="div-border"></div>
        <Footer />
        <Modal modalOpen={this.state.modalOpen} handleClose={() => this.setState({modalOpen: false})} />
      </div>
      </MuiThemeProvider>
    );
  }
}

Ficha.propTypes = {
  sheet: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    sheet: state.sheet
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(SheetActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Ficha);
