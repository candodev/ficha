import {combineReducers} from 'redux';
import todos from './todos';
import sheet from './sheet';
import hexagons from './hexagons';

const rootReducer = combineReducers({
  todos,
  sheet,
  hexagons
});

export default rootReducer;
