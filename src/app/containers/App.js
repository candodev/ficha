import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Header} from './../components/potencia/header';
import MainSection from '../components/MainSection';
import * as TodoActions from '../actions/index';
import classNames from 'classnames';
import {browserHistory} from 'react-router';

import './style.scss';

const hexagons = [
  {
    url: "url('/assets/images/subjectbrowse.png')",
    text: 'Exploración',
    link: '/#/path?level=Primaria&grade=1&lesson=Exploración&bimester=1',
    className: 'subject-hex-green',
    grade: '1º grado',
    level: 'PRIMARIA'
  },
  {
    url: "url('/assets/images/subjectesp.png')",
    text: 'Español',
    link: '/#/path?level=Primaria&grade=6&lesson=Espa%C3%B1ol&bimester=1',
    className: 'subject-hex-sky',
    grade: '6º grado',
    level: 'PRIMARIA'
  },   
  {
    url: "url('/assets/images/mate.png')",
    text: '',
    link: '/#/path?level=Primaria&grade=3&lesson=Matem%C3%A1ticas&bimester=1',
    className: 'subject-hex-orangemath',
    grade: '3º grado',
    level: 'PRIMARIA'
  },
  {
    url: "url('/assets/images/subjectmath.png')",
    text: 'Matemáticas',
    link: '/#/path?level=Secundaria&grade=3&lesson=Matemáticas&bimester=1',
    className: 'subject-hex-orange',
    grade: '3º grado',
    level: 'SECUNDARIA'
  }
];

class App extends Component {
  constructor(props) {
    super(props);
    this.handleSendTo = this.handleSendTo.bind(this);
  }

  handleSendTo(a) {
    browserHistory.push();
  }

  createMarkup(htmlContent) {
    return {__html: htmlContent};
  }

  render() {
    let hexagonsSplit = [];

    for (var i = 0; i < hexagons.length; i+=6) {
      let to = null;
      if(i + 6 < hexagons.length) {
        to = i + 6;
      } else {
        to = hexagons.length;
      }

      hexagonsSplit.push(hexagons.slice(i, to).map((element, index) => {
        let dialogClass = classNames('subject-hex', 'cursor-pointer', element.className);
        return (
          <div className="hexagon-wrapper" style={{marginRight: '90px'}} key={index} onClick={this.handleSendTo.bind(this, element.text)}>
            <a target="_blank" href={element.link}>
              <div className={dialogClass} style={{backgroundImage: element.url}}>
                <div className="hexLeft"></div>
                <div className="hexRight"></div>
                <div className="subject-name" dangerouslySetInnerHTML={this.createMarkup(element.text)}></div>
              </div>
            </a>
            <p style={{color: '#E3437B', fontSize: '15px'}}>
              {element.grade}<br/>
              <span style={{color: 'black', fontWeight: '300'}}>{element.level}</span>
            </p>
          </div>
        );
      }));
    }
    
    return (
      <MuiThemeProvider>
        <div>
          <Header query={location.query} />
          <div className="textures">
            <div className="welcome">
              <h1>bienvenido a la<br/>plataforma de potencia educativa</h1>
              <p style={{margin: '-10px 0px 60px 0px'}}>Descubre todo lo que Potencia tiene para ofrecer a tu clase, clic en la materia y grado que te interesa conocer.</p>
              <div className="hexagon-row">{hexagonsSplit}</div>
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  todos: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    todos: state.todos
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(TodoActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
