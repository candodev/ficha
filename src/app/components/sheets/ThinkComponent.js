import React, {Component, PropTypes} from 'react';

class Think extends Component {
  constructor(props, context) {
    super(props, context);
  }

  createMarkup(htmlContent) {
    return {__html: htmlContent};
  }

  render() {
    const content = this.props.thinkContent;
    return (
      <div>
        <div className="paragraph-style">
          <div dangerouslySetInnerHTML={this.createMarkup(content.text_student)}></div>
        </div>
        <div className="grey-square">
          <h3 className="title-margin">Idea </h3>
          <div dangerouslySetInnerHTML={this.createMarkup(content.text_teacher)}></div>
        </div>
        <div className="yellow-square">
          <img src="assets/images/temporary/icon.png" className="img-icon" alt="ejemplo" />
          <h4 className="title-mg">MÁS IDEAS EN CONTENIDO SOLO PARA WEB</h4>
          {content.content ? content.content.map((embed, index) => {
            return(
              <div key={index}>
                <div dangerouslySetInnerHTML={this.createMarkup(embed.idea_additional_text)}></div>
                <h4>ARCHIVOS ADJUNTOS</h4>
                <ul>
                  <li>
                    <a target="_blank" href={embed.idea_additional_files}>{embed.idea_additional_files}</a>
                  </li>
                </ul>
              </div>
            )
          }) : null}
        </div>
      </div>
    );
  }
}

Think.propTypes = {
  thinkContent: PropTypes.object.isRequired
};

export default Think;
